'use strict'

const fastify = require('fastify')(),
    mongodb = require('fastify-mongodb'),
    formbody = require('fastify-formbody'),
    helmet = require('fastify-helmet'),
    mongooseConfig = require('./config/db.config'),
    api = require('./routers/api'),
    port = process.env.PORT || 3000

let err;

fastify.register(mongodb, mongooseConfig, err => {
    if (err) throw err
})

fastify.register(formbody, {}, (err) => {
    if (err) throw err
})

fastify.register(helmet, {}, (err) => {
    if (err) throw err
})

fastify.register(api, {
    prefix: '/api'
}, (err) => {
    if (err) throw err
})

process.on('uncaughtException', (err) => {
    console.error('uncaughtException: ', err.message);
    console.error(err.stack);
});

fastify.listen(port, (err) => {
    if (err) throw err
    console.log(`server listening on ${fastify.server.address().port}`);
})