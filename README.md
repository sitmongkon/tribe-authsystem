# Tribe Authentication Server

Authentication Server for Tribe AR/VR Game

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Node.js ^8.0.0
```

### Installing

A step by step series of examples that tell you have to get a development env running

Step 1 Install the dependencies
```
$ npm install
```

Or If you want to use yarn

```
$ yarn
```

Step 2 Start the project
```
$ npm start
```

Or

```
$ yarn start
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Node.js](https://nodejs.org/) - The server framework used
* [Fastify](https://www.fastify.io/) - The web framework used
* [MongoDB](https://www.mongodb.com/) - The nosql database used

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Reawpai Chunsoi** - *Lead Programmer* - [Phaicom](https://github.com/Phaicom)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
