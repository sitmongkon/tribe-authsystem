module.exports = (fastify, options, next) => {
    fastify.get('/:id', (req, reply) => {
        const {
            db
        } = fastify.mongo
        db.collection('account', onCollection)

        function onCollection(err, col) {
            if (err) return next(err)

            col.findOne({
                id: +req.params.id
            }, (err, user) => {
                if (user == null)
                    user = {
                        message: `user ${req.params.id} doesn't exist!`
                    }
                else if (err)
                    throw err

                reply.send(user)
            })
        }
    })
    next();
}