require('dotenv').config()
const URL = process.env.MONGODB_URL,
    PORT = process.env.MONGODB_PORT,
    ID = process.env.USERNAME,
    PWD = process.env.PASSWORD,
    DB = process.env.DB

const mongooseConfig = {
    url: `mongodb://${ID}:${PWD}@${URL}:${PORT}/${DB}`
}

module.exports = mongooseConfig