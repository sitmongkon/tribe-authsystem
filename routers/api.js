const users = require('./users')
const auth = require('./auth')

module.exports = (fastify, options, next) => {
    fastify.decorateReply('error', function (payload) {
        this.code(payload.code)
            .send({
                statusCode: payload.code,
                message: payload.message
            })
    })

    fastify.get('/', (req, res) => {
        res.send({
            hello: 'world'
        })
    })

    fastify.register(auth, (err) => {
        if (err) throw err
    })

    fastify.register(users, {
        prefix: '/user'
    }, (err) => {
        if (err) throw err
    })

    next();
}