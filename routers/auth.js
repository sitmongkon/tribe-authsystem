const moment = require('moment');

module.exports = (fastify, options, next) => {
    fastify.post('/register', (req, reply) => {
        let username = req.body.username
        let password = req.body.password

        const {
            db
        } = fastify.mongo
        db.collection('accounts', onCollection)

        function onCollection(err, col) {
            if (err) return next(err)

            col.findOne({
                username: username,
                password: password
            }, (err, exists) => {
                if (err) return next(err)

                if (exists)
                    return reply.error({
                        code: 400,
                        message: 'account already registered!'
                    })

                let account = {
                    username: username,
                    password: password,
                    email: 'test@test.com',
                    rank: 1500,
                    registerDate: moment().format('DDMMYYYYHHmmss'),
                    avatarId: 'A001'
                }

                col.insertOne(account, (err, ret) => {
                    if (err) return next(err)

                    col.findOne({
                        username: username,
                        password: password
                    }, (err, account) => {
                        if (err) return next(err)

                        // delete password
                        delete account.password

                        reply.code(200)
                            .header('Content-Type', 'application/json')
                            .send({
                                statusCode: 200,
                                message: 'ok',
                                account: account
                            })
                    })

                })
            })
        }
    })

    fastify.post('/getData', (req, reply) => {
        let username = req.body.username;
        let password = req.body.password;

        const {
            db
        } = fastify.mongo
        db.collection('accounts', onCollection)

        function onCollection(err, col) {
            if (err) return next(err)

            col.findOne({
                username: username,
                password: password
            }, (err, account) => {
                if (err) return next(err)

                if (!account)
                    return reply.error({
                        code: 400,
                        message: `account doesn't exist!`
                    })

                // delete password
                delete account.password

                reply.code(200)
                    .header('Content-Type', 'application/json')
                    .send({
                        statusCode: 200,
                        message: 'ok',
                        account: account
                    })
            })
        }
    })

    next();
}